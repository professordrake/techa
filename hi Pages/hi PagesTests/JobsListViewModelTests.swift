//
//  JobsListViewModelTests.swift
//  hi PagesTests
//
//  Created by Shamas on 28/9/20.
//

import XCTest
@testable import hi_Pages

class JobsListViewModelTests: XCTestCase {

    var viewModel: JobsListViewModel!
    
    override func setUpWithError() throws {
        viewModel = JobsListViewModel(store: MockDependencyProvider.getStore())
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }

    func testViewModel() throws {
        XCTAssertEqual(viewModel.numberOfJobs, 0)
        
        let dataUpdatedExpectation = XCTestExpectation(description: "onDataUpdated")
        viewModel.onDataUpdated = {
            dataUpdatedExpectation.fulfill()
        }
        viewModel.requestData()
        wait(for: [dataUpdatedExpectation], timeout: 5)
        XCTAssertEqual(viewModel.numberOfJobs, 3)
        
        let jobTypeChangeExpectation = XCTestExpectation(description: "onJobsTypeChanged")
        viewModel.onJobsTypeChanged = { jobType in
            jobTypeChangeExpectation.fulfill()
        }
        viewModel.jobTypeSelectionToggled()
        wait(for: [jobTypeChangeExpectation], timeout: 5)
        XCTAssertEqual(viewModel.numberOfJobs, 1)
        
        XCTAssertNotNil(viewModel.getJobCellViewModel(index: 0))
    }


}
