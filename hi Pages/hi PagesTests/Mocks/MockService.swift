//
//  MockService.swift
//  hi PagesTests
//
//  Created by Shamas on 28/9/20.
//

import Foundation
@testable import hi_Pages

class MockService: WebService {
    func fetch(urlRequest: Routable, completionHandler: @escaping (Result<JobsResponse, ServiceError>) -> Void) {
        
    }
}
