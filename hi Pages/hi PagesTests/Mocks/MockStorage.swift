//
//  MockStorage.swift
//  hi PagesTests
//
//  Created by Shamas on 28/9/20.
//

import Foundation
@testable import hi_Pages

class MockStorage: Storage {
    var jobs: [JobModel] = []
    
    func saveJobs(jobs: [JobModel]) {
        self.jobs = jobs
    }
    
    func getAllJobs() -> [JobModel] {
        return jobs
    }
    
}
