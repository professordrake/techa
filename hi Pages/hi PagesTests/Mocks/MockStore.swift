//
//  MockStore.swift
//  hi PagesTests
//
//  Created by Shamas on 28/9/20.
//

import Foundation
@testable import hi_Pages

class MockStore: Store {
    
    var mockContentData: Data {
        return getData(name: "mockResponse")
    }
    
    func getData(name: String, withExtension: String = "json") -> Data {
        let bundle = Bundle(for: type(of: self))
        let fileUrl = bundle.url(forResource: name, withExtension: withExtension)
        let data = try! Data(contentsOf: fileUrl!)
        return data
    }
    
    func requestData(callBack: @escaping (Result<[JobModel], DataFetchError>) -> Void) {
        let data = getData(name: "mockResponse")
        let response = try? JSONDecoder().decode(JobsResponse.self, from: data)
        callBack(.success(response?.jobs ?? []))
    }
    
    func getAllJobs() -> [JobModel] {
        return []
    }
    
}
