//
//  MockDependencyProvider.swift
//  hi PagesTests
//
//  Created by Shamas on 28/9/20.
//

import Foundation
@testable import hi_Pages

class MockDependencyProvider: DependencyProvider {
    
    static func getStore() -> Store {
        return MockStore()
    }
    
    static func getStorage() -> Storage {
        return MockStorage()
    }
    
    static func getRouter() -> Routable {
        return JobsRouter()
    }
    
    static func getService() -> WebService {
        return MockService()
    }
    
    
}
