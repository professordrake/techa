//
//  Date+Extension.swift
//  hi Pages
//
//  Created by Shamas on 26/9/20.
//

import Foundation

extension Date {
    
    static let numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .ordinal
        return numberFormatter
    }()
    
    static let dateFormatter: DateFormatter = {
       let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    static let displayDateFomatter: DateFormatter = {
       let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter
    }()
    
    static func getDateFromString(dateString: String) -> Date? {
        return dateFormatter.date(from: dateString)
    }
    
    func getStringFromDate() -> String {
        return Date.dateFormatter.string(from: self)
    }
    
    func getDisplayFormattedSting() -> String {
        let day = Calendar.current.component(.day, from: self)
        let dayString = Date.numberFormatter.string(from: NSNumber(value: day)) ?? ""
        let monthYearString = Date.displayDateFomatter.string(from: self)
        return  dayString + " " + monthYearString
    }
}
