//
//  UIView+Extension.swift
//  hi Pages
//
//  Created by Shamas on 26/9/20.
//

import UIKit

extension UIView {
    
    static let orangeBorderColor = "orangeBorderColor"
    
    // should switch with a better alternate, such as enum
    static let borderTag = 112233
    
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func addOrangeBottomBorder() {
        addBottomBorder(color: UIColor(named: UIView.orangeBorderColor), andWidth: 2)
    }
    
    func removeOrangeBottomBorder() {
        viewWithTag(UIView.borderTag)?.removeFromSuperview()
    }
    
    func addBottomBorder( color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.tag = UIView.borderTag
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        border.frame = CGRect(x: 0, y: frame.size.height - borderWidth, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }
}
