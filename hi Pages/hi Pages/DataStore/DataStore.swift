//
//  DataStore.swift
//  hi Pages
//
//  Created by Shamas on 26/9/20.
//

import Foundation

enum DataFetchError: Error {
    case dataFetchFailed
}

protocol Store {
    
    /// Rquests the data from
    /// - Parameter callBack: Completion Handler with Result type
    func requestData(callBack: @escaping (Result<[JobModel], DataFetchError>) -> Void)
    
    /// Gets all the jobs saved locally through Storage
    func getAllJobs() -> [JobModel]
}

class DataStore: Store {
    
    let storage: Storage
    let service: WebService
    let router: Routable
    
    init(storage: Storage, service: WebService, router: Routable) {
        self.storage = storage
        self.service = service
        self.router = router
    }
    
    func requestData(callBack: @escaping (Result<[JobModel], DataFetchError>) -> Void) {
        service.fetch(urlRequest: router) { (result) in
            switch (result) {
            case .success(let jobsResponse):
                self.handleJobsFetched(jobs: jobsResponse.jobs)
                callBack(.success(self.getAllJobs()))
            case .failure(let error):
                print(error)
                callBack(.failure(DataFetchError.dataFetchFailed))
            }
        }
    }
    
    func handleJobsFetched(jobs: [JobModel]) {
        addToStorage(jobs: jobs)
    }
    
    private func addToStorage(jobs: [JobModel]) {
        self.storage.saveJobs(jobs: jobs)
    }
    
    func getAllJobs() -> [JobModel] {
        return storage.getAllJobs()
    }
    
}
