//
//  DependencyProvider.swift
//  hi Pages
//
//  Created by Shamas on 24/9/20.
//

import Foundation

protocol DependencyProvider {
    
    static func getStore() -> Store
    
    static func getStorage() -> Storage
    
    static func getRouter() -> Routable
    
    static func getService() -> WebService
}

class JobsDependencyProvider: DependencyProvider {
    
    static func getStore() -> Store {
        return DataStore(storage: JobsDependencyProvider.getStorage(),
                         service: JobsDependencyProvider.getService(),
                         router: JobsDependencyProvider.getRouter())
    }
    
    static func getStorage() -> Storage {
        return DataStorage()
    }
    
    static func getRouter() -> Routable {
        return JobsRouter()
    }
    
    static func getService() -> WebService {
        return JobsService()
    }
}
