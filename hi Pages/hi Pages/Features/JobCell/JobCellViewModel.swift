//
//  JobCellViewModel.swift
//  hi Pages
//
//  Created by Shamas on 26/9/20.
//

import Foundation

class JobCellViewModel {
    
    let jobModel: JobModel
    
    init(model: JobModel) {
        jobModel = model
    }
    
    var connectedBusinesses: [ConnectedBusinessModel] {
        return jobModel.connectedBusinesses ?? []
    }
    
    
    var numberOfHiredBusinesses: Int {
        return ((jobModel.connectedBusinesses ?? []).filter({ $0.isHired })).count
    }
    
    var rowHeight: Int {
        let count = jobModel.connectedBusinesses?.count ?? 0
        print("xxx count \(count)")
        return 240 + jobModel.getArrayForBusinesses(itemsPerRow: 4).count * 80
    }
    
    func closeButtonTapped() {
        //Improvement: Handle on close job
    }
    
    func getJobViewModel() -> JobViewModel {
        return JobViewModel(model: jobModel)
    }
}
