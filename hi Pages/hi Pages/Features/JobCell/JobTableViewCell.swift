//
//  JobTableViewCell.swift
//  hi Pages
//
//  Created by Shamas on 26/9/20.
//

import UIKit

class JobTableViewCell: UITableViewCell {

    static let cellIdentifier = "jobCellIdentifier"
    static let borderGrayColor = "borderGrayColor"
    
    @IBOutlet weak var jobView: JobView!
    
    var viewModel: JobCellViewModel! {
        didSet {
            updateView()
        }
    }
    
    override func prepareForReuse() {
        jobView.prepareForReuse()
    }
        
    func updateView() {
        
        layer.borderColor = UIColor(named: JobTableViewCell.borderGrayColor)?.cgColor
        layer.borderWidth = 2
        
        jobView.onCloseButtonTapped = { [weak self] in
            self?.viewModel.closeButtonTapped()
        }
        
        jobView.viewModel = viewModel.getJobViewModel()
        
    }
}
