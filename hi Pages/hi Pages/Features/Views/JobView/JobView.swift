//
//  JobView.swift
//  hi Pages
//
//  Created by Shamas on 26/9/20.
//

import UIKit

class JobView: UIView {
    
    
    @IBOutlet var contentView: JobView!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var datePostedLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var businessDetailLabel: UILabel!
    @IBOutlet weak var viewDetails: UIButton!
    @IBOutlet weak var hiredBusinessView: UIView!
    @IBOutlet weak var hiredBusinessHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet var tapGestureRecognizer: UITapGestureRecognizer!
    
    var onCloseButtonTapped: (() -> Void)?
    
    var viewModel: JobViewModel! {
        didSet {
            updateView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    private func initialize() {
        Bundle.main.loadNibNamed("JobView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        setupCloseButton()
        tapGestureRecognizer.isEnabled = false
    }
    
    
    fileprivate func setupLabels() {
        jobTitle.text = viewModel.category
        datePostedLabel.text = viewModel.datePostedString
        statusLabel.text = viewModel.jobStatus
        businessDetailLabel.text = viewModel.businessDetail
    }
    
    fileprivate func getVerticalStackView() -> UIStackView {
        let hiredFrame = hiredBusinessView.frame
        let verticalStackView = UIStackView(frame: CGRect(x: 0, y: 0, width: hiredFrame.width, height: hiredFrame.height))
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fillEqually
        verticalStackView.alignment = .center
        return verticalStackView
    }
    
    fileprivate func setupVerticalStackViewConstraints(stackView: UIStackView) {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.widthAnchor.constraint(equalTo: hiredBusinessView.widthAnchor, multiplier: 1).isActive = true
        stackView.heightAnchor.constraint(equalTo: hiredBusinessView.heightAnchor, multiplier: 1).isActive = true
        stackView.centerYAnchor.constraint(equalTo: hiredBusinessView.centerYAnchor).isActive = true
        stackView.centerXAnchor.constraint(equalTo: hiredBusinessView.centerXAnchor).isActive = true
    }
    
    fileprivate func getHorizontalStackView() -> UIStackView {
        let hiredFrame = hiredBusinessView.frame
        let horizontalStack = UIStackView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: hiredFrame.width, height: hiredFrame.height)))
        horizontalStack.axis = .horizontal
        horizontalStack.distribution = .equalSpacing
        horizontalStack.translatesAutoresizingMaskIntoConstraints = false
        return horizontalStack
    }
    
    func updateView() {
        setupLabels()
        
        let businessRowArray = viewModel.getArrayForBusinesses()
        
        let heightOfARow: CGFloat = BusinessView.viewHeight
        let totalHeight: CGFloat = CGFloat(businessRowArray.count) * heightOfARow
        hiredBusinessHeightConstraint.constant = totalHeight
        let verticalStackView = getVerticalStackView()
        hiredBusinessView.addSubview(verticalStackView)
        setupVerticalStackViewConstraints(stackView: verticalStackView)
        
        for anIndex in 0..<businessRowArray.count {
            let horizontalStack = getHorizontalStackView()
            verticalStackView.addArrangedSubview(horizontalStack)
            
            let businessRow = businessRowArray[anIndex]
            
            for businessIndex in 0..<businessRow.count {
                let businessView = BusinessView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: heightOfARow, height: heightOfARow)))
                businessView.widthAnchor.constraint(equalToConstant: heightOfARow).isActive = true
                businessView.heightAnchor.constraint(equalToConstant: heightOfARow).isActive = true
                businessView.viewModel = BusinessViewModel(model: businessRow[businessIndex])
                horizontalStack.addArrangedSubview(businessView)
            }
        }
    }
    
    func prepareForReuse() {
        hiredBusinessView.subviews.forEach({ $0.removeFromSuperview() })
        menuButton.isSelected = true
        menuButtonPressed(menuButton)
    }
    
    private func setupCloseButton() {
        closeButton.layer.borderColor = UIColor.lightGray.cgColor
        closeButton.layer.cornerRadius = 4
        closeButton.layer.borderWidth = 2
        closeButton.isHidden = true
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        onCloseButtonTapped?()
    }
    
    @IBAction func userTapped(_ sender: UITapGestureRecognizer) {
        menuButtonPressed(menuButton)
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        menuButton.isSelected = !menuButton.isSelected
        tapGestureRecognizer.isEnabled = menuButton.isSelected
        closeButton.isHidden = !menuButton.isSelected
    }
    
}
