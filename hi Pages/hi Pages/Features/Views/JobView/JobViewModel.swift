//
//  JobViewModel.swift
//  hi Pages
//
//  Created by Shamas on 28/9/20.
//

import Foundation

enum JobDisplayText: String {
    case postedDate = "posted"
}

class JobViewModel {
    static let itemsPerRows: Int = 4
    let jobModel: JobModel
    
    init(model: JobModel) {
        jobModel = model
    }
    
    var category: String {
        return jobModel.category
    }
    
    var datePostedString: String {
        let postedString = String(format: NSLocalizedString(JobDisplayText.postedDate.rawValue, comment: ""), (jobModel.postedDate?.getDisplayFormattedSting() ?? ""))
        return postedString
    }
    
    var jobStatus: String {
        return jobModel.jobStatus.displayString
    }

    func getArrayForBusinesses() -> [[ConnectedBusinessModel]] {
        return jobModel.getArrayForBusinesses(itemsPerRow: JobViewModel.itemsPerRows)
    }
    
    var businessDetail: String {
        let count = (jobModel.connectedBusinesses?.filter({ $0.isHired }))?.count ?? 0
        let formatString = NSLocalizedString("business count message", comment: "")
        return String.localizedStringWithFormat(formatString, count)
    }

}
