//
//  BusinessViewModel.swift
//  hi Pages
//
//  Created by Shamas on 27/9/20.
//

import Foundation

class BusinessViewModel {
    
    let businessModel: ConnectedBusinessModel
    
    init(model: ConnectedBusinessModel) {
        businessModel = model
    }
    
    var isHired: Bool {
        return businessModel.isHired
    }
    
    var businessURL: URL? {
        return businessModel.thumbnailURL
    }
    
}
