//
//  BusinessView.swift
//  hi Pages
//
//  Created by Shamas on 27/9/20.
//

import UIKit
import Kingfisher

class BusinessView: UIView {
    
    static let viewHeight: CGFloat = 80
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var isHiredLabel: UILabel!

    var viewModel: BusinessViewModel? {
        didSet {
            updateView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    private func initialize() {
        
        Bundle.main.loadNibNamed("BusinessView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func updateView() {
        guard let vm = viewModel else {
            return
        }
        imageView.kf.setImage(with: vm.businessURL)
        isHiredLabel.isHidden = !vm.isHired
        contentView.bringSubviewToFront(isHiredLabel)
    }

}
