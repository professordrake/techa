//
//  JobsListViewModel.swift
//  hi Pages
//
//  Created by Shamas on 26/9/20.
//

import Foundation

class  JobsListViewModel {
    
    private var store: Store
    private var allJobs: [JobModel] = [] {
        didSet {
            filteredJobs = getJobsForCurrentSelection(typeSelected: jobsTypeSelected)
            onDataUpdated?()
        }
    }
    
    private var filteredJobs: [JobModel] = []
    
    private var jobsTypeSelected: JobStatus = .inProgress {
        didSet {
            filteredJobs = getJobsForCurrentSelection(typeSelected: jobsTypeSelected)
            onDataUpdated?()
            onJobsTypeChanged?(jobsTypeSelected)
        }
    }
    
    var onDataUpdated: (() -> Void)?
    var onJobsTypeChanged: ((JobStatus) -> Void)?
    
    init(store: Store) {
        self.store = store
    }
    
    var numberOfJobs: Int {
        return filteredJobs.count
    }
    
    /// Toggles between selected Job Type (Open Jobs/Closed Jobs)
    func jobTypeSelectionToggled() {
        jobsTypeSelected = jobsTypeSelected == .inProgress ? .closed : .inProgress
        
    }
    
    private func getJobsForCurrentSelection(typeSelected: JobStatus) -> [JobModel] {
        return allJobs.filter({ $0.jobStatus ==  typeSelected })
    }
    
    /// Makes the initial request for Data Fetch
    func requestData() {
        store.requestData {[weak self] (result) in
            switch result {
            case .success(let jobs):
                self?.allJobs = jobs
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func getJobForIndex(index: Int) -> JobModel? {
        guard index >= 0 && index < filteredJobs.count else {
            return nil
        }
        return filteredJobs[index]
    }
    
    /// Creates the JobCellViewModel for the given Index
    /// - Parameter index: Index
    /// - Returns: JobCellViewModel (nullable)
    func getJobCellViewModel(index: Int) -> JobCellViewModel? {
        guard let jobModel = getJobForIndex(index: index) else {
            return nil
        }
        return JobCellViewModel(model: jobModel)
    }
    
}
