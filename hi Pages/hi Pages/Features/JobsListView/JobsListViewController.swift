//
//  ViewController.swift
//  hi Pages
//
//  Created by Shamas on 24/9/20.
//

import UIKit

class JobsListViewController: UIViewController {

    let cellSeparatorHeight: CGFloat = 8
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var openJobsButton: UIButton!
    @IBOutlet weak var closedJobsButton: UIButton!
    
    var viewModel: JobsListViewModel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        let store = JobsDependencyProvider.getStore()
        viewModel = JobsListViewModel(store: store)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        setupTableView()
        buttonSelectedForTag(tag: 1)
    }
    
    func setupViewModel() {
        viewModel.onDataUpdated = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.onJobsTypeChanged = { [weak self] jobType in
            self?.buttonSelectedForTag(tag: jobType == JobStatus.inProgress ? 1 : 2)
        }
        
        viewModel.requestData()
    }
    
    @IBAction func jobsButtonTapped(_ sender: UIButton) {
        viewModel.jobTypeSelectionToggled()
    }
    
    func buttonSelectedForTag(tag: Int) {
        let buttonToSelect = tag == 1 ? openJobsButton : closedJobsButton
        let buttonToDeSelect = tag == 1 ? closedJobsButton : openJobsButton
        changeButtonState(button: buttonToSelect, isSelected: true)
        changeButtonState(button: buttonToDeSelect, isSelected: false)
    }
    
    func changeButtonState(button: UIButton?, isSelected: Bool) {
        button?.isSelected = isSelected
        if isSelected {
            button?.addOrangeBottomBorder()
        } else {
            button?.removeOrangeBottomBorder()
        }
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
}

extension JobsListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height = viewModel.getJobCellViewModel(index: indexPath.row)?.rowHeight ?? 44
        print("xxx height \(height)")
        return CGFloat(height)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfJobs
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: JobTableViewCell.cellIdentifier, for: indexPath)
        
        if let jobCell = cell as? JobTableViewCell {
            jobCell.viewModel = viewModel.getJobCellViewModel(index: indexPath.row)
        }
        
        return cell
    }
    
}

