//
//  JobsResponse.swift
//  hi Pages
//
//  Created by Shamas on 24/9/20.
//

import Foundation

struct JobsResponse: Codable {
    let jobs: [JobModel]
}
