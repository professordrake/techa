//
//  ConnectedBusinessModel.swift
//  hi Pages
//
//  Created by Shamas on 24/9/20.
//

import Foundation

// MARK: - ConnectedBusiness
struct ConnectedBusinessModel: Codable {
    let businessID: Int
    let thumbnail: String
    let isHired: Bool

    enum CodingKeys: String, CodingKey {
        case businessID = "businessId"
        case thumbnail, isHired
    }
    
    var thumbnailURL: URL? {
        return URL(string: thumbnail)
    }
}
