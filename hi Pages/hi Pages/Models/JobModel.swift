//
//  JobModel.swift
//  hi Pages
//
//  Created by Shamas on 24/9/20.
//

import Foundation

enum JobStatus: String {
    case inProgress = "in progress"
    case closed = "closed"
    case unknown
    
    var displayString: String {
        switch (self) {
        case .inProgress:
            return String(format: NSLocalizedString(self.rawValue, comment: "") )
        case .closed:
            return String(format: NSLocalizedString(self.rawValue, comment: "") )
        case .unknown:
            return "Unknown"
        }
    }
}

// MARK: - Job
struct JobModel: Codable {
    let jobID: Int
    let category, postedDateString, status: String
    let connectedBusinesses: [ConnectedBusinessModel]?
    let detailsLink: String

    enum CodingKeys: String, CodingKey {
        case jobID = "jobId"
        case postedDateString = "postedDate"
        case category, status, connectedBusinesses, detailsLink
    }
    
    var postedDate: Date? {
        return Date.getDateFromString(dateString: postedDateString)
    }
    
    var jobStatus: JobStatus {
        return JobStatus(rawValue: status.lowercased()) ?? .unknown
    }
    
    func getArrayForBusinesses(itemsPerRow: Int) -> [[ConnectedBusinessModel]] {
        var array: [[ConnectedBusinessModel]] = []
        guard let businesses = connectedBusinesses else {
            return array
        }
        
        let numberOfItemsInRow: Float = Float(itemsPerRow)
        let objectsCount = Float(businesses.count)
        let numberOfRows = Int(ceil(objectsCount / numberOfItemsInRow))
        for _ in 0..<numberOfRows {
            array.append([])
        }
        
        for index in 0..<(connectedBusinesses?.count ?? 0) {
            let arrayIndex = index / Int(numberOfItemsInRow)
            array[arrayIndex].append(businesses[index])
        }
        
        return array
    }
}
