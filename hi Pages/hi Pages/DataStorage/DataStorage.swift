//
//  DataStorage.swift
//  hi Pages
//
//  Created by Shamas on 25/9/20.
//

import Foundation
import CoreData

protocol Storage {
    
    
    /// Saves the list of Jobs to local storage
    /// - Parameter jobs: Array of Jobs to save
    func saveJobs(jobs: [JobModel])
    
    
    /// Fetches the list of Jobs that have been saved locally
    func getAllJobs() -> [JobModel]
}

class DataStorage: Storage {
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "hi_Pages")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print(error.userInfo)
            }
        })
        return container
    }()
    
    func saveJobs(jobs: [JobModel]) {
        for aJob in jobs {
            
            let existingJobs = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Job.self))
            existingJobs.predicate = NSPredicate(format: "jobId = %d", aJob.jobID)
            
            do {
                let allJobs = try? persistentContainer.viewContext.fetch(existingJobs) as? [Job]
                if let existingJob = allJobs?.first {
                    existingJob.populateFromJobModel(jobModel: aJob)
                } else {
                    if let jobObj = NSEntityDescription.insertNewObject(forEntityName: "Job", into: persistentContainer.viewContext) as? Job {
                        jobObj.populateFromJobModel(jobModel: aJob)
                    }
                }
                saveContext()
            }
        }
    }
    
    func getAllJobs() -> [JobModel] {
        
        var fetchedJobs: [Job] = []
        let jobsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Job.self))

        do {
            fetchedJobs = try persistentContainer.viewContext.fetch(jobsFetch) as! [Job]
        } catch {
            print("Failed to fetch jobs: \(error)")
        }
        
        return fetchedJobs.map({ $0.getJobModel() })
    }

    // MARK: - Core Data Saving support

    private func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                print("error \(nserror)")
            }
        }
    }
}
