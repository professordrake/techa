//
//  Business+CoreDataProperties.swift
//  hi Pages
//
//  Created by Shamas on 28/9/20.
//
//

import Foundation
import CoreData


extension Business {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Business> {
        return NSFetchRequest<Business>(entityName: "Business")
    }

    @NSManaged public var businessId: Int32
    @NSManaged public var isHired: Bool
    @NSManaged public var thumbnail: String?
    @NSManaged public var job: Job?

}

extension Business : Identifiable {

}
