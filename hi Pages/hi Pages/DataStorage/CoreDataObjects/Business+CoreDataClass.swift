//
//  Business+CoreDataClass.swift
//  hi Pages
//
//  Created by Shamas on 28/9/20.
//
//

import Foundation
import CoreData


public class Business: NSManagedObject {
    func getBusinessModel() -> ConnectedBusinessModel {
            let business = ConnectedBusinessModel(businessID: Int(businessId), thumbnail: thumbnail ?? "", isHired: isHired)
            return business
        }
        
        func populateFromBusinessModel(businessModel: ConnectedBusinessModel) {
            businessId = Int32(businessModel.businessID)
            thumbnail = businessModel.thumbnail
            isHired = businessModel.isHired
        }
}
