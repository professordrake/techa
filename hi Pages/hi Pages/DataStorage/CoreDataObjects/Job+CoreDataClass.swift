//
//  Job+CoreDataClass.swift
//  hi Pages
//
//  Created by Shamas on 28/9/20.
//
//

import Foundation
import CoreData


public class Job: NSManagedObject {
    
    func getJobModel() -> JobModel {
        var businesses: [ConnectedBusinessModel] = []
        for aBusiness in (connectedBusinesses ?? []) where aBusiness is Business {
            if let aBusinessObj = aBusiness as? Business {
                businesses.append(aBusinessObj.getBusinessModel())
            }
        }
        let job: JobModel = JobModel(jobID: Int(self.jobId), category: self.category ?? "",
                                     postedDateString: postedDate?.getStringFromDate() ?? "", status: self.status ?? "",
                                     connectedBusinesses: businesses, detailsLink: self.detailsLink ?? "")
        return job
    }
        
    func populateFromJobModel(jobModel: JobModel) {
        jobId = Int32(jobModel.jobID)
        category = jobModel.category
        postedDate = jobModel.postedDate
        status = jobModel.status
        detailsLink = jobModel.detailsLink
        
        removeAllBusiness()
        
        for aBusiness in jobModel.connectedBusinesses ?? [] {
            if let context = self.managedObjectContext {
                if let businessCD = NSEntityDescription.insertNewObject(forEntityName: "Business", into: context) as? Business {
                    businessCD.populateFromBusinessModel(businessModel: aBusiness)
                    addToConnectedBusinesses(businessCD)
                }
            }
        }
    }
    
    func removeAllBusiness() {
        guard let allBusiness = connectedBusinesses else {
            return
        }
        
        for anObject in allBusiness {
            if let aBusiness = anObject as? Business {
                removeFromConnectedBusinesses(aBusiness)
            }
        }
    }
}
