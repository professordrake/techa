# hiPages

hiPages uses given API, fetches the jobs, stores them locally to CoreData and shows them to users grouped by their status.

Due to time constraints, close button functionality has not yet been implemented.

## Developer Concerns
* Job status string in JSON is not consistent. (`In Progress` vs `In progress`). Right now we are creating enums from their lower cased version.
* The UI Design is a bit Android-esque material design inspired.
* The close button overlay for Kebab Menu (3 vertical dots) is not iOS recommended.
(https://developer.apple.com/design/human-interface-guidelines/ios/views/popovers/)

## Design Considerations

### MVVM
The app uses MVVM to separate the business logic from UI. `JobsListViewModel` is the responsible for the first opening screen. This VM initiates requests the data from `DataStore`, which uses `JobsService` to get data from backend, and stores it to CoreData using `DataStorage`.

### Dependency Inversion
Instead of relying on concrete classes, we use Protocol Oriented Programming, which help us abide by the Dependency Inversion principle of SOLID principles. For instance, `DataStore` implements `Store` protocol, and `DataStorage` implements `Storage`. 

Later if we want to replace our CoreData implementation, by some other data storage mechanism, we only have to switch out DataStorage, by the newer implementation without needing to change code anywhere else.

### UI Setup
As much as possible, we have setup the views independently, so that they can be reused in other parts of the app as well. For instance, `JobView` is built from its separate xib, and is inserted into the `JobTableViewCell`. 

Similarly, `BusinessView` is built from its own xib, and can be used elsewhere as well. 

### String Management
Strings are mostly being lifted from their localised versions.
For plural handling (1 business, 2 businesses) we are using `Localizable.stringsdict`.

### Network

Network calls rely on 2 protocols, `Routable` and `WebService`. Splitting these two up, helps us decouple routing information from the network call logic. Their concrete implementation are `JobsRouter` and `JobsService`. 

## Unit Testing
For now we have unit testing in place for `JobsListViewModel`. To setup the unit tests, we have setup Mock objects for Store (`MockStore`), WebService(`MockService`) etc.

## Improvements
* CoreData inserts can be changed to batch inserts.
* The top buttons (Open Jobs/Closed Jobs) should be made into a reusable component.
* Add more unit tests independently testable ViewModels
